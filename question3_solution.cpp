#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <stack>

#define RESET "\033[0m"
#define RED "\033[31m"

using namespace std;

class graph
{
    vector<vector<int>> grid_map;

public:
    // instantiating class
    graph(vector<vector<int>> grid_map)
    {
        this->grid_map = grid_map;
    }

    // print the solution
    void print_solution(vector<pair<int, int>> path)
    {
        cout << "Solution: ";
        for (int i = 0; i < path.size(); i++)
        {
            grid_map[path[i].first][path[i].second] = 3;
            cout << "( " << path[i].first << ", " << path[i].second << " )";
            if (i != path.size() - 1)
                cout << " -> ";
        }
        cout << endl;
        for (auto x : grid_map)
        {
            for (auto y : x)
            {
                if (y == 3)
                    cout << RED << y << " " << RESET;
                else
                    cout << y << " ";
            }
            cout << endl;
        }
        cout << "0s in the grid map represents the obstacle" << endl;
        cout << "1s in the grid map represents unexplored but viable path" << endl;
        cout << "2s in the grid map represents nodes / vertices that has been explored by the BFS algorithm" << endl;
        cout << "3s in the grid map represents the selected shortest path from starting point to destination" << endl;
    }

    // using Breadth First Search (BFS) to find the shortest path from start to dest
    void find_path(pair<int, int> start, pair<int, int> dest)
    {
        // validating inputs
        int row = grid_map.size();
        int col = grid_map[0].size();

        for (auto x : grid_map)
        {
            if (x.size() != col)
            {
                cout << "Grid map's size not consistent" << endl;
                return;
            }
        }
        try
        {
            if (!grid_map.at(start.first).at(start.second) || !grid_map.at(dest.first).at(dest.second))
            {
                cout << "Starting and destination can't be obstacle" << endl;
                return;
            }
        }
        catch (const out_of_range &e)
        {
            cout << "Starting and destination must be valid" << endl;
            return;
        }

        // start populating variables and initiate BFS
        grid_map[start.first][start.second] = 2;
        map<pair<int, int>, pair<int, int>> prev;
        queue<pair<int, int>> visit_queue;

        visit_queue.push(start);
        prev[start] = {-1, -1};
        while (!visit_queue.empty())
        {

            pair<int, int> curr = visit_queue.front();
            visit_queue.pop();

            // exploring in the sequence of up, down left and right
            vector<int> col_dir = {0, 0, -1, 1};
            vector<int> row_dir = {1, -1, 0, 0};

            for (int i = 0; i < col_dir.size(); i++)
            {
                int rr = curr.first + row_dir[i];
                int cc = curr.second + col_dir[i];
                if (rr < 0 || cc < 0 || rr > row - 1 || cc > col - 1)
                    ;
                else if (rr == dest.first && cc == dest.second)
                {
                    // solution found, backtracking vertices
                    prev[{rr, cc}] = curr;

                    stack<pair<int, int>> reversed_path;

                    reversed_path.push({rr, cc});
                    int prev_x = rr;
                    int prev_y = cc;

                    while (!(prev[{rr, cc}] == make_pair(-1, -1)))
                    {
                        reversed_path.push(prev[{rr, cc}]);
                        pair<int, int> temp = prev[{rr, cc}];
                        rr = temp.first;
                        cc = temp.second;
                    }
                    vector<pair<int, int>> path;
                    while (!reversed_path.empty())
                    {
                        path.push_back(reversed_path.top());
                        reversed_path.pop();
                    }

                    // printing solution nicely
                    this->print_solution(path);
                    return;
                }

                else if (grid_map[rr][cc] == 1)
                {
                    visit_queue.push(make_pair(rr, cc));
                    grid_map[rr][cc] = 2;
                    prev[{rr, cc}] = curr;
                }
            }
        }

        // no solution found :(
        cout << "No valid path found" << endl;
        return;
    }
};

int main()
{
    vector<vector<int>> grid_map{
        {1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 0, 0, 1, 1},
        {1, 1, 1, 0, 0, 1, 1},
        {1, 1, 1, 0, 0, 1, 1},
        {1, 1, 1, 0, 0, 1, 1},
        {1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1},
    };

    pair<int, int> start = make_pair(3, 1);
    pair<int, int> dest = make_pair(4, 5);

    graph g(grid_map);
    g.find_path(start, dest);
    return 0;
}