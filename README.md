# Dyson Interview Question 3 Proposed Solution #

A qeustion regarding robot path finding with obstacle.  
Given occupancy map, starting location and destination, print out the shortest path for the robot to reach the destination.

### Algorithms ###
This problem can be broken down to a graph problem.  
Breadth First Search (BFS) is used to find the shortest path between 2 points in a graph.  
From the starting point, neighbouring non-obstacle and not yet explored vertices are added to a queue(in a sequence of up, down, left and right). A vertex is marked explored by changing the grid map value to 2.   
During exploring, a hashtable will hold the coordinate of previous vertex, this is for backtracking purposes later.  
Continue exploring the vertex on the front of the queue and popping it.  
Once solution are found, backtrack the previous vertices until the origin is reached. Backtracked vertices are marked 3 and coloured red in the grid map.  
Solution is not found once all vertices are explored and dest is still not reached. 


### Output ###
Given the problem:  
![Photo of problem](sample_output/problem1.jpg)  
The output of the solution:  
![Photo of solution](sample_output/output1.jpg)    

### Build and Tests ###
To change the grid map occupancy and obstacle, please edit the grid_map vector under main;  
To change the starting location, please edit the start pair under main;  
To change the destination location, please edit the dest pair under main;  

After editing, the code can be built with:   
```g++ -o question3_solution.exe question3_solution.cpp```  
For best experience, use VS Code's build and debug feature (F5).

### Limitation and Assumption ###
1. The grid map must only contain 0 or 1
2. The grid map size need not to be square, but the number of element for each column and row must be the same.
3. Starting point and destination must be valid and can't be obstacle

